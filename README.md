# Links

[Itch.io (stable build)](https://greenf0x.itch.io/mandelbrot) 

[GitLab Pages (test build)](https://greenfox.gitlab.io/godot-mandelbrot)

# TODO (maybe)

- fullscreen toggle
- change colors/themes
  - theme editor
- framerate counter
- ~~touch (mobile) controls~~
- screenshot saver
- linkable locations (big share)
- rotation support
- better precision
