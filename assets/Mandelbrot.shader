shader_type canvas_item;
render_mode skip_vertex_transform;

uniform highp vec2 resolution; 


uniform highp vec2 center; 
uniform highp float hue; 
uniform highp vec2 zoom; 
//uniform vec4 color:hint_color;
uniform sampler2D samplerTexture;
uniform int maxItterations;

const int TRUE_MAX_RUNS = 1024;

varying highp vec2 c;



// VectorFunc:4
vec4 rgb2hsv(vec4 input)
{
	vec4 K = vec4(0.0, -1.0 / 3.0, 2.0 / 3.0, -1.0);
	vec4 p = mix(vec4(input.bg, K.wz), vec4(input.gb, K.xy), step(input.b, input.g));
	vec4 q = mix(vec4(p.xyw, input.r), vec4(input.r, p.yzx), step(p.x, input.r));
	float d = q.x - min(q.w, q.y);
	float e = 1.0e-10;
	return vec4(abs(q.z + (q.w - q.y) / (6.0 * d + e)), d / (q.x + e), q.x,input.a);
}

// VectorFunc:2
vec4 hsv2rgb(vec4 input)
{
	vec4 K = vec4(1.0, 2.0 / 3.0, 1.0 / 3.0, 3.0);
	vec3 p = abs(fract(input.xxx + K.xyz) * 6.0 - K.www);
	return vec4(vec3(input.z * mix(K.xxx, clamp(p - K.xxx, 0.0, 1.0), input.y)),input.a);
}

//f(z+1) = z^2 +c
//f(0) = 0
int Mandelbrot()
{
	highp vec2 z = c;
	highp vec2 C2;
	for(int i = 0; i < TRUE_MAX_RUNS;i++){
		C2.x = z.x * z.x;
		C2.y = z.y * z.y;
		z.y = (2.0 * z.x * z.y) + c.y;
		z.x = C2.x-C2.y+c.x;
		if((C2.x+C2.y)>4.0){
			return i;
		}
		if(i >= maxItterations){
			return 0;
		}
	}
	return 0;
}

void vertex(){
	VERTEX.xy = UV.xy * resolution; //* 0.5 + (resolution*0.25);

	c = (UV-0.5)*zoom - center;
//	c = topLeft - vec2(UV.x * botRight.x,UV.y * botRight.y);
}


void fragment(){
	highp float magnintue = min(float(maxItterations),float(TRUE_MAX_RUNS));
	highp float a = float(Mandelbrot())/magnintue;
	COLOR = texture(samplerTexture,vec2(a,0));
	COLOR = hsv2rgb(rgb2hsv(COLOR) + vec4(hue,0,0,0));
//	COLOR = vec4(color.rgb * a,1);
//	COLOR = vec4(c,0,1);
}