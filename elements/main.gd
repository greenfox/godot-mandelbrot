tool
extends Sprite


export var zoomRate= 0.1;

export var resolution:Vector2 setget setResolution

#shader vars
export var zoom:float = -7.5 setget setZoom
export var itterations:int = 1000 setget setItters
export var center:Vector2 = Vector2(0.75,0) setget setCenter
export var color:Color setget setColor
export var hue:float setget setHue
export(Texture) var samplerTexture setget setTexture ;

var trueZoom:Vector2;

var mouseRelative := Vector2.ZERO
var state = {}
var _os2own = {}


func _process(delta):
	var reletiveMotion := Vector2.ZERO
	if state.size():
		if state.has_all([0,1]):
			var a = state[0]
			var b = state[1]
			var so = ((b.p - b.d) - (a.p - a.d)).length()
			var se = (b.p - a.p).length()
			var sdelta = se - so
			a.d = Vector2.ZERO
			b.d = Vector2.ZERO
			
			setZoom(zoom - (sdelta*8)/resolution.length())
		elif state.has(0):
			reletiveMotion += state[0].d
			state[0].d = Vector2.ZERO
	reletiveMotion += mouseRelative
	mouseRelative = Vector2.ZERO;
	setCenter( center + (trueZoom*reletiveMotion*1.5) / OS.window_size)

	setZoom(zoom) #I guess? to make sure the window hasn't changed shape?
	setResolution(0);
	if Input.is_action_just_pressed("p"):
		if $AnimationPlayer.is_playing():
			$AnimationPlayer.stop()
		else:
			$AnimationPlayer.play("pulse")
			
func setResolution(value):
	var t;
	if get_viewport():
		t = get_viewport().size
	else:
		t = OS.window_size
	resolution = t;
	get_material().set_shader_param("resolution",resolution)
	
	
func setZoom(value):
	var t;
	if get_viewport():
		t = get_viewport().size
	else:
		t = OS.window_size
	
	zoom = value;
	zoom = clamp(zoom,-24,0)

	trueZoom = Vector2.ONE * pow(2,zoom-1) * t;
#	print(z);
	get_material().set_shader_param("zoom",trueZoom)

func setCenter(value):
	center = value;
	get_material().set_shader_param("center",center)

func setColor(value):
	color = value;
	get_material().set_shader_param("color",color)
	
func setTexture(value):
	samplerTexture = value
	get_material().set_shader_param("samplerTexture",samplerTexture)

func setHue(value):
	hue = value
	get_material().set_shader_param("hue",hue)

func setItters(value):
	itterations = value
	get_material().set_shader_param("maxItterations",itterations)
	

signal toggleVisiable
func _unhandled_input(event):
	if event is InputEventScreenTouch:
		if event.pressed: # Down.
			if !_os2own.has(event.index): # Defensively discard index if already known.
				var ptr_id = _find_free_pointer_id()
				state[ptr_id] = {p=event.position,d=Vector2.ZERO}
				_os2own[event.index] = ptr_id
				if ptr_id == 2:
					emit_signal("toggleVisiable")
		else: # Up.
			if _os2own.has(event.index): # Defensively discard index if not known.
				var ptr_id = _os2own[event.index]
				state.erase(ptr_id)
				_os2own.erase(event.index)
		get_tree().set_input_as_handled()
		
	elif event is InputEventScreenDrag: # Movement.
		if _os2own.has(event.index): # Defensively discard index if not known.
			var ptr_id = _os2own[event.index]
			state[ptr_id].p = event.position
			state[ptr_id].d += event.relative
		get_tree().set_input_as_handled()
		
	elif event is InputEventMouseMotion and (event.button_mask & 1) and state.size() == 0:
		if Input.get_mouse_mode() == Input.MOUSE_MODE_CAPTURED:
			mouseRelative= event.relative
		get_tree().set_input_as_handled()
		
	elif event is InputEventMouseButton and state.size() == 0:
		if(event.button_index == 1):
			if(event.pressed):
				Input.set_mouse_mode(Input.MOUSE_MODE_CAPTURED);
			else:
				Input.set_mouse_mode(Input.MOUSE_MODE_VISIBLE);
		elif(event.button_index == 4):
			zoom -= zoomRate * event.factor;
		elif(event.button_index == 5):
			zoom += zoomRate * event.factor;
		get_tree().set_input_as_handled()
	elif event is InputEventKey:
		pass

func _find_free_pointer_id():
	var used = state.keys()
	var i = 0
	while i in used:
		i += 1
	return i



func _on_UI_changeHue(value):
	
	pass # Replace with function body.
